import time
import hashlib
import json
import os
from multiprocessing import Pool

import boto3
from pymongo import MongoClient


def get_mongo_collection():
    mongo_client = MongoClient()
    db = mongo_client.tourico_data
    collection = db.logs

    return collection


def get_hash(event_data):
    m = hashlib.md5()
    m.update(
        "{}-{}-{}".format(
            event_data["req_time"],
            event_data["action"],
            event_data["fluentd_time"],
        )
    )
    return m.hexdigest()


def upsert_event(event):
    event_data = json.loads(event["message"])
    unique_hash = get_hash(event_data)
    get_mongo_collection().update({"pid": unique_hash}, {"$set": event_data},
                                  upsert=True)


def fetch_from_the_start(stream, start=None, end=None, next_token=None):
    print "Fetching logs from stream: {}, pid: {}".format(stream, os.getpid())

    if not start:
        start = int(time.time()) - (86400 * 7)

    boto_client = boto3.client('logs', region_name="eu-west-1")
    query_data = {
        "logGroupName": "coral.investigation",
        "logStreamName": stream,
        "startTime": start,
        "limit": 100,
        "startFromHead": True
    }

    if next_token:
        query_data.update({"nextToken": next_token})

    response = boto_client.get_log_events(**query_data)

    print " >> Upserting events. ({}) [{}]".format(len(response["events"]),
                                                  stream)
    for event in response["events"]:
        upsert_event(event)

    if response.get("nextForwardToken"):
        print " >> Fetching next page:", response.get("nextForwardToken")
        if response.get("nextForwardToken") != next_token:
            fetch_from_the_start(
                stream,
                next_token=response.get("nextForwardToken")
            )
        else:
            print " >> Pagination stopped for stream:", stream


if __name__ == '__main__':
    p = Pool(4)  # let's use cpu cores efficient.
    streams = ["child-{}-agent".format(i) for i in range(1, 17)]
    for stream in streams:
        p.apply_async(fetch_from_the_start, (stream,))
    p.close()
    p.join()
